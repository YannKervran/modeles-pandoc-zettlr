---
unelettre: true # Indiquer qu'il s'agit d'une lettre (ne rien changer ici)
mainfont: "Liberation Serif" # Sélection de la police principale (verifier que la police est installée sur le système)
sansfont: "Liberation Sans" # sélection de la police sans serif utilisée en cas de mise en page "business" (verifier que la police est installée sur le système)
#taillepolice: "18pt" # spécifier la taille de la police voulue (sinon, ne rien mettre, par défaut la taille est à 12pt).
#interligne: "2" # spécifier l'interligne (sinon, ne rien mettre, par défaut l'interligne est à 1.15).

fromname: "Prénom Nom" # Nom et prénom de l'expéditeur (si non renseigné, rien ne s'affiche)
fromphone: "03699955456" # Numéro de téléphone de l'expéditeur (si non renseigné, rien ne s'affiche)
fromemail: "bidule@truc.fr" # adresse courriel de l'expéditeur (si non renseigné, rien ne s'affiche)
fromurl: "http://truc.muche.fr" # URL de l'expéditeur (si non renseigné, rien ne s'affiche)
#backaddress: "Adresse de retour" # Adresse de retour si différente de l'adresse expédition

fromaddress: true # Afficher l'adresse de l'expéditeur (dès lors, renseigner les champs suivants)
fromaddresstreet: "X nom de la voie" # numéro et voie expéditeur
fromaddresszip: "xxxxxx" # Code postal de l'expéditeur
fromaddresscity: "Ville" # Commune de l'expéditeur

recipient: true # Afficher nom et adresse du destinataire (dès lors, renseigner les champs suivants)
recipientname: "Marcel Dupont" # Nom du destinataire
#recipientothername: "Momo" # Nom du destinataire si on souhaite utiliser une variante (si non renseigné, c'est la variable recipientname qui s'applique) 
recipienstreet: "5 avenue DuMessy" # numéro et voie du destinataire
recipientzip: "99099" # Code postal du destinataire
recipientcity: "Villedest" # Ville du destinataire

#place: "Un Lieu-Dit" # Lieu d'écriture de la lettre (si cette ligne est commentée ou omise, c'est la commune de l'adresse d'expédition fromaddresscity qui est utilisée).

date: "le \\today" # ou bien mettez la date que vous voulez, de la forme que vous voulez

objetlettre: "la lettre a un objet" # Renseigner ici l'objet de la lettre (si non renseigné, rien ne s'affiche)

adressepolie: "Cher Monsieur" # Ici renseigner : Cher Monsieur, Chère Madame, Salut, Coucou.... (si non renseigné, rien ne s'affiche)

#signature: "Signé Fantomas" # Contenu de la signature (si cette ligne est commentée ou omise, c'est la variable fromname qui est utilisée)

# Renseigner les trois items suivants modifiera la mise en page avec un aspect "business"
#myref: "5558-554B" # Numéro référence de l'expéditeur
#yourref: "KKU-556932B" # Numérode de référence du destinataire
#invoice: "fact-20210315" # Numéro de facture

closing: "Formule de politesse" # Fin de la lettre (si non renseigné, rien ne s'affiche)

#postscriptum: "Ceci est un post-scriptum" # le p.s. (si non renseigné, rien ne s'affiche)

#attach: "Noms de documents joints" # Liste des documents joints (si non renseigné, rien ne s'affiche)

#copiea: "Machin Bidule et Truc Muche" # Liste des destinataires en copie (si non renseigné, rien ne s'affiche)
...


Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ullamcorper convallis massa, ac feugiat erat gravida vel. Fusce egestas lacus eu accumsan viverra. Aliquam sodales urna at odio tincidunt luctus. Nulla vel posuere diam. Integer eleifend cursus ipsum, ac lobortis odio volutpat non. Pellentesque at molestie justo. Aenean nec fermentum enim. Aliquam erat nunc, fermentum interdum ex non, tincidunt commodo magna. Maecenas tortor elit, viverra a congue ut, pulvinar ut lacus. In hac habitasse platea dictumst. Fusce sit amet dolor nisl. Nulla a velit at diam laoreet pellentesque nec ut velit. Nullam et odio lorem. Donec ac ligula at elit aliquet blandit non ac risus. Morbi accumsan vel lacus quis tristique.

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.


Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus. 
