# Templates Framatophe pour Pandoc (et/ou Zettlr)

Voici trois modèles (templates) pour Pandoc / Zettlr : Article (académique), Lettre et Rapport. Les modèles utilisent KOMA-Script et les classes ``scrartcl`` (article), ``scrlttr2``  (lettre) et ``scrreprt`` (rapport).

Tout cela fonctionne plus ou moins bien, bricolez un peu si besoin !

## Prérequis

- Pandoc
- distribution TeX (par exemple : TexLive)
- éventuellement Zettlr

Attention, chaque modèle est différent et comporte des options et des comamndes très différentes. Il faut donc vous assurer que les en-têtes des fichiers markdow que vous créez correspondent bien au modèle que vous voulez employer. Ppour vous aider, les fichier ``malettre.md``, ``monrapport.md``, ``monarticle.md``, ``mondocument.md`` présentent les en-têtes prêtes à être modifiées ou complétées. 

Prêtez attention aux commentaires dans les en-têtes : ils sont là pour vous aider à modifier correctement les paramètres de mise en page.

## Comment utiliser le modèle lettre ?

Les options se remplissent dans l'en-tête Yaml du document Markdown. Utilisez le fichier ``malettre.md`` dans ce dépôt et lisez simplement les commentaires.

Attention : avec la variable ``mainfont`` il s'agit de choisir une police présente sur votre système d'exploitation, entrez correctement son nom, elle sera utilsiée avec le moteur de compilation XeLaTeX.

Ensuite, avec Pandoc :

```
pandoc malettre.md --pdf-engine=xelatex --template=modele-lettre.tex -o malettre.pdf``
```

Ou bien si vous utilisez Zettlr, allez dans ``Fichier > Préférences > Paramètres de PDF`` et renseignez le champ ``Modèle Tex Personalisé`` (avec ``modele-lettre.tex``). Exportez ensuite votre document au format PDF.


## Comment utiliser le modèle article ?

Ce modèle est librement (et finalement lointainement) inspiré du modèle [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) pour pandoc.

Ce modèle utilise seulement les styles CSL pour la bibliographie. Cela signifie que vous pouvez utiliser un fichier de bibliographie au format ``bibtex`` ou ``json`` mais il faudra créer un [style CSL](https://citationstyles.org/) ou (mieux) aller piocher un style CSL dans [ce dépôt](https://editor.citationstyles.org/searchByName/). Je vous conseille le [style français aux normes ISO 690](https://editor.citationstyles.org/styleInfo/?styleId=http%3A%2F%2Fwww.zotero.org%2Fstyles%2Fiso690-author-date-fr-no-abstract).

Les options se remplissent dans l'en-tête Yaml du document Markdown. Utilisez le fichier ``monarticle.md`` dans ce dépôt et lisez simplement les commentaires.

Attention : avec la variable ``mainfont`` il s'agit de choisir une police présente sur votre système d'exploitation, entrez correctement son nom, elle sera utilsiée avec le moteur de compilation XeLaTeX.


### Avec Zettlr

- Renseigner dans Zettlr le modèle ``.tex`` (``modele-article.tex``) dans les options PDF (``Fichier > Préférences > Paramètres de PDF`` et renseignez le champ ``Modèle Tex Personalisé``.),
- Renseigner bien sûr le style CSL et la biblio dans Zettlr.

### Avec pandoc seul

La ligne de commandes est la suivante (d'autres commandes et options existent avec pandoc, à vous de voir) :

```
pandoc monarticle.md -s --pdf-engine=xelatex --bibliography=fichierbiblio.json --filter pandoc-citeproc --csl=votremodeleCSL.csl --template=modele-article.tex --mathjax --toc -o production-article.pdf

```

Remplacez :

- ``monarticle.md`` par le nom de fichier de votre article au format markdown
- ``fichierbiblio.json`` par le nom de fichier de votre bibliographie
- ``votremodeleCSL.csl`` par le nom de votre fichier de style CSL
- ``production-article.pdf`` par le nom de fichier attendu à la sortie PDF

## Comment utiliser le modèle Rapport ?

Ce modèle est similaire au modèle Article. Il utiliser la classe ``scrreprt`` de Koma-script. Il est optimisé par défaut sur ``[twoside]``, c'est-à-dire pour une impression recto-verso (marges de gauche et marges de droite). 

Si vous utilisez un logo, pensez à bien indiquer le nom du fichier et éventuellement le chemin.

### Avec Zettlr

- Renseigner dans Zettlr le modèle ``.tex`` (``modele-rapport.tex``) dans les options PDF (``Fichier > Préférences > Paramètres de PDF`` et renseignez le champ ``Modèle Tex Personalisé``.),
- Renseigner bien sûr le style CSL et la biblio dans Zettlr, si besoin.


### Avec pandoc seul

La ligne de commandes est la suivante (d'autres commandes et options existent avec pandoc, à vous de voir) :

```
pandoc monrapport.md -s --pdf-engine=xelatex --bibliography=fichierbiblio.json --filter pandoc-citeproc --csl=votremodeleCSL.csl --template=modele-rapport.tex --mathjax --toc -o production-rapport.pdf

```

## Pas envie de changer à chaque fois ? Utilisez le modèle Fouzitou !

Le modèle Fouzitou est l'assemblage des modèles ci-dessus. Il vous permet d'éviter de changer à chaque fois le modèle dans les options de Zettlr ou dans la ligne de commande pandoc.

Utilisez simplement les fichiers markdown qui correspondent au type de production que vous voulez faire (document, lettre, rapport, article) et compilez avec ``modele-fouzitou.tex``.

### Avec Zettlr

- Renseigner dans Zettlr le modèle ``.tex`` (``modele-fouzitou.tex``) dans les options PDF (``Fichier > Préférences > Paramètres de PDF`` et renseignez le champ ``Modèle Tex Personalisé``.),
- Renseigner bien sûr le style CSL et la biblio dans Zettlr, si besoin.


### Avec pandoc seul

La ligne de commandes est la suivante (d'autres commandes et options existent avec pandoc, à vous de voir) :

```
pandoc xxxxxxx.md -s --pdf-engine=xelatex --bibliography=fichierbiblio.json --filter pandoc-citeproc --csl=votremodeleCSL.csl --template=modele-fouzitou.tex --mathjax --toc -o production-xxxxxx.pdf

```

et remplacez ``xxxxxxx.md`` par le fichier voulu comportant les en-têtes YAML correspondant au type de document que vous voulez produire.


## Comment modifier les modèles ?

J'ai tâché de commenter et arranger lisiblement les modèles TeX. Vous pouvez donc les bricoler pour ajuster ces modèles à vos besoins.

Le parti pris est de ne pas mélanger les genres : là où Eisvogel (et les template pandoc par défaut) proposent de traiter aussi bien une présentation (Beamer), un livre ou un article avec le même template, je pense qu'il est plus logique de changer de modèle à chaque fois et cibler correctement le besoin.

## Illustrations

![Modèle de lettre](lettrepage.jpg)

![Modèle d'article 1](articlepage1.jpg)

![Modèle d'article 2](articlepage2.jpg)

![Modèle d'article 3](articlepage3.jpg)
 

![Modèle de rapport 1](rapportpage1.jpg)

