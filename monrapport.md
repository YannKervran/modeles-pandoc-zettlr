---
unrapport: true # indiquer qu'il s'agit d'un rapport (ne rien changer ici)
# Ci dessous, renseignez les champs, et mettez bien les valeurs entre guillemets
documentclass: report # report ou book ont pour effet de passer la hiérarchies des titres de niveau 1 à Chapter ou Part. En fait... ne touchez à rien ici.
lang: "fr-FR" # renseigner la langue du doc (obligatoire)
title: "Lorem Ipsum lacinia hendrerit" # renseigner le titre (obligatoire)
small-title: "Lorem Ipsum" # version courte du titre pour les en-têtes (obligatoire)
author: "Pierre-Jean Sérien" # Renseigner l'auteur (obligatoire)
date: "01 avril 2021" # Renseigner la date (obligatoire)
version: "V. 0.96" # Version du document
info-sup: "Fusce tellus rhoncus at rhoncus \\ Quisque eget neque in nisi tincidunt mollis" # Renseigner les infos supplémentaires qui figureront sur la page de titre, vers le bas de la page
logo: "une-image.png" # renseigner le nom de fichier du logo (astuce avec Zettlr : placer l'image dans le dossier de travail courant et glissez l'image depuis la barre de droite vers l'emplacement, vous aurez le chemin)
titlepage: true # true or false : si on veut ou non une page de titre
toc: true # si on veut ou non une table des matières
toc-title: "Table des matières" # renommer la table des matières
toc-own-page: true # si on veut que la table des matières ai sa propre page
toc-depth: 4 # niveaux de titres dans la table des matières (n'oubliez pas qu'en réduisant les niveaux, cela devient un sommaire :))
#lof: true # Liste des figures 
#lot: true # Liste des tables
couleur1: "725794"  # couleur hexadecimale sans le croisillon (noir par défaut)
couleur2: "cc4e13"  # couleur hexadecimale sans le croisillon (noir par défaut)
couleur3: "4a5568"  # couleur hexadecimale sans le croisillon (noir par défaut)
numbersections: true # Si on numérote les sections
links-as-notes: true
link-citations: false # avec hyperref
colorlinks: true # avec hyperref
#linkcolor: magenta # avec hyperref (on peut changer les couleurs mais c'est déconseillé)
#urlcolor: blue # avec hyperref (on peut changer les couleurs mais c'est déconseillé)
#citecolor: blue # avec hyperref (on peut changer les couleurs mais c'est déconseillé)
mainfont: "Liberation Serif" # Sélection de la police principale (vérifier que la police est installée sur le système!)
#mainfontoptions: "Scale=1.0" # échelle de la police (pas vraiment nécessaire)
sansfont: "Noto Sans" # sélection de la police sans serif (vérifier que la police est installée sur le système!)
monofont: "Noto Sans" # sélection de la police mono (vérifier que la police est installée sur le système!)
# mathfont: "xxxxxxxxxxx" # sélection de la police pour maths  (vérifier que la police est installée sur le système!)
fontsize: 12pt # taille de la police
#linestretch: 1 # interligne (par défaut : 1.1)
...


# Premier chapitre avec un titre bien long qui contient une courgette et un melon

Nulla suscipit eleifend pretium. Vivamus dolor velit, sagittis ac dolor sed, venenatis scelerisque dui. Nam nec urna tempus, dictum dui non, sodales augue. Aenean tempor convallis urna eu dignissim. Duis dictum tortor a lectus imperdiet, vitae malesuada nisi maximus. Nunc mattis pellentesque dui, id vulputate purus. Phasellus erat dui, lacinia nec felis et, imperdiet bibendum velit.

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. [Phasellus](http://lien.com) eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

## Première section

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit [@atten2013].

### Une sous-sous-section


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. 


```
Un peu de verbatim, cela rend la vie plus belle.

```

Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam [@medina2011]. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.

#### Une sous-sous-sous-section

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam[@medina2011]. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.


### Une sous-sous-section

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam[@medina2011]. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.


## Seconde section

Nulla suscipit eleifend pretium. Vivamus dolor velit, sagittis ac dolor sed, venenatis scelerisque dui. Nam nec urna tempus, dictum dui non, sodales augue. Aenean tempor convallis urna eu dignissim. Duis dictum tortor a lectus imperdiet, vitae malesuada nisi maximus. Nunc mattis pellentesque dui, id vulputate purus. Phasellus erat dui, lacinia nec felis et, imperdiet bibendum velit.

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. Phasellus eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

> Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis.


Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.




| Légumes   | Légumineux | Céréales |
|-----------|------------|----------|
| Navet     | Lentilles  | Blé      |
| Courgette | Haricots   | Seigle   |


Table: On ajoute la légende de la table de cette manière.



Voyez [comment faire des tableaux en pandoc-markdown](https://pandoc.org/MANUAL.html#tables).
Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.



Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

# Second chapitre

Sed felis risus, ornare ut tellus id, rutrum sagittis felis. Nunc dictum ligula sit amet vehicula molestie. Fusce quis dignissim justo. Mauris fermentum sodales enim. Ut sed pretium est, sit amet sodales massa. Sed varius, nunc nec bibendum congue, est augue luctus justo, at lacinia justo tortor eu massa. In ligula ex, mollis non sagittis nec, congue quis nulla.

## Première section

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.

Nulla suscipit eleifend pretium. Vivamus dolor velit, sagittis ac dolor sed, venenatis scelerisque dui. Nam nec urna tempus, dictum dui non, sodales augue. Aenean tempor convallis urna eu dignissim. Duis dictum tortor a lectus imperdiet, vitae malesuada nisi maximus. Nunc mattis pellentesque dui, id vulputate purus. Phasellus erat dui, lacinia nec felis et, imperdiet bibendum velit.

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. Phasellus eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

Sed felis risus, ornare ut tellus id, rutrum sagittis felis. Nunc dictum ligula sit amet vehicula molestie. Fusce quis dignissim justo. Mauris fermentum sodales enim. Ut sed pretium est, sit amet sodales massa. Sed varius, nunc nec bibendum congue, est augue luctus justo, at lacinia justo tortor eu massa. In ligula ex, mollis non sagittis nec, congue quis nulla.

## Seconde section

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.

Nulla suscipit eleifend pretium. Vivamus dolor velit, sagittis ac dolor sed, venenatis scelerisque dui. Nam nec urna tempus, dictum dui non, sodales augue. Aenean tempor convallis urna eu dignissim. Duis dictum tortor a lectus imperdiet, vitae malesuada nisi maximus. Nunc mattis pellentesque dui, id vulputate purus. Phasellus erat dui, lacinia nec felis et, imperdiet bibendum velit.

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. Phasellus eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

Sed felis risus, ornare ut tellus id, rutrum sagittis felis. Nunc dictum ligula sit amet vehicula molestie. Fusce quis dignissim justo. Mauris fermentum sodales enim. Ut sed pretium est, sit amet sodales massa. Sed varius, nunc nec bibendum congue, est augue luctus justo, at lacinia justo tortor eu massa. In ligula ex, mollis non sagittis nec, congue quis nulla.



# Troisième chapitre

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. Phasellus eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

## Première section

Sed felis risus, ornare ut tellus id, rutrum sagittis felis. Nunc dictum ligula sit amet vehicula molestie. Fusce quis dignissim justo. Mauris fermentum sodales enim. Ut sed pretium est, sit amet sodales massa. Sed varius, nunc nec bibendum congue, est augue luctus justo, at lacinia justo tortor eu massa. In ligula ex, mollis non sagittis nec, congue quis nulla.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.

Nulla suscipit eleifend pretium. Vivamus dolor velit, sagittis ac dolor sed, venenatis scelerisque dui. Nam nec urna tempus, dictum dui non, sodales augue. Aenean tempor convallis urna eu dignissim. Duis dictum tortor a lectus imperdiet, vitae malesuada nisi maximus. Nunc mattis pellentesque dui, id vulputate purus. Phasellus erat dui, lacinia nec felis et, imperdiet bibendum velit.

Aliquam lacus libero, sagittis nec justo eget, volutpat sodales purus. Aliquam at lobortis tortor. Nullam eget sem eros. Aenean tempor volutpat sapien, et fringilla tortor pulvinar eget. Aenean tincidunt vehicula quam, sed scelerisque magna volutpat ut. Phasellus eu leo in justo dignissim varius non iaculis diam. Sed ut ornare ex. Fusce vestibulum massa eget sem tempus posuere. Phasellus auctor purus vitae ex auctor efficitur.

## Seconde section

Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

Sed felis risus, ornare ut tellus id, rutrum sagittis felis. Nunc dictum ligula sit amet vehicula molestie. Fusce quis dignissim justo. Mauris fermentum sodales enim. Ut sed pretium est, sit amet sodales massa. Sed varius, nunc nec bibendum congue, est augue luctus justo, at lacinia justo tortor eu massa. In ligula ex, mollis non sagittis nec, congue quis nulla.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat tortor vitae justo porta hendrerit. Cras porta magna ac augue venenatis sollicitudin. Mauris auctor euismod congue. Nulla ultricies tortor id magna tempus, quis vehicula est tincidunt. Maecenas id ultrices justo. Integer eu pellentesque mauris, nec luctus justo. Phasellus eget placerat quam. Cras at mauris finibus, sollicitudin neque vulputate, molestie est. Etiam eu diam blandit ligula posuere elementum in vel sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce tellus arcu, rhoncus at rhoncus et, dignissim et mi. Quisque eget neque in nisi tincidunt mollis vitae luctus dolor. Nam non ante diam. Curabitur ac tristique enim. Integer leo arcu, blandit a venenatis vel, accumsan a ligula. Nulla eget justo metus.



Nunc feugiat rhoncus sem at laoreet. Cras vitae ullamcorper libero, vel fringilla justo. Mauris nec accumsan justo. Etiam pharetra odio nunc, et porttitor odio vulputate non. Aliquam convallis erat magna, nec fringilla enim aliquam non. Maecenas est nulla, iaculis eget rutrum vitae, lacinia non arcu. Vestibulum vestibulum lacinia nulla sed ultrices. Curabitur efficitur odio ac tellus lacinia hendrerit. Sed mattis ante non dolor mattis lacinia. Donec gravida tortor pulvinar, porta diam at, porttitor orci. Quisque lorem velit, faucibus ac tempor ut, vehicula in sem. Duis iaculis euismod leo, vel vestibulum enim pulvinar blandit.

# Références

